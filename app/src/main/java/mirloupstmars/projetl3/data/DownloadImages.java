package mirloupstmars.projetl3.data;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

import mirloupstmars.projetl3.data.model.ImageModel;

public class DownloadImages extends AsyncTask<String, Void, ArrayList<ImageModel>> {
    @Override
    protected ArrayList<ImageModel> doInBackground(String... urls) {
        ArrayList<ImageModel> images = new ArrayList<>();
        for (String urlString : urls) {
            try {
                URL url = new URL(urlString);
                HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
                InputStream inputStream = connection.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) stringBuilder.append(line).append('\n');
                JSONArray jsonArray = new JSONArray(stringBuilder.toString());

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jo = jsonArray.getJSONObject(i);
                    images.add(new ImageModel(jo.getString("download_url")));
                }
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
        }
        return images;
    }
}
