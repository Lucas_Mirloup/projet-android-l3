package mirloupstmars.projetl3.data;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;

import mirloupstmars.projetl3.data.model.LoggedInUser;
import mirloupstmars.projetl3.data.model.User;

/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of login status and user credentials information.
 */
public class LoginRepository {
    private LoginDao loginDao;

    public LoginRepository(Application application) {
        AppDatabase db = AppDatabase.getDatabase(application);
        loginDao = db.loginDao();
    }

    private void insertUser(User user) {
        new LoginInsertion(loginDao).execute(user);
    }

    @SuppressWarnings("unchecked")
    public Result<LoggedInUser> login(String username, String password) {
        Result<LoggedInUser> result;
        try {
            User user = loginDao.getUser(username);
            if (user == null) {
                Log.e("login", "new user");
                User newUser = new User();
                newUser.uid = username;
                newUser.passwordHash = password;
                insertUser(newUser);
            } else if (!password.equals(user.passwordHash)) {
                Log.e("login", "wrong password");
                throw new Exception();
            }
            LoggedInUser fakeUser = new LoggedInUser(username);
            result = new Result.Success<>(fakeUser);
        } catch (Exception e) {
            result = new Result.Error(new IOException("Error logging in", e));
        }
        return result;
    }

    private static class LoginInsertion extends AsyncTask<User, Void, Void> {
        private LoginDao loginDao;

        private LoginInsertion(LoginDao loginDao) {
            this.loginDao = loginDao;
        }

        @Override
        protected Void doInBackground(User... users) {
            loginDao.insertUsers(users[0]);
            return null;
        }
    }
}