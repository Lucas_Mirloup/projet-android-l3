package mirloupstmars.projetl3.data;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import mirloupstmars.projetl3.data.model.User;

@Dao
public interface LoginDao {
    @Query("SELECT * FROM user")
    LiveData<List<User>> getAllUsers();

    @Query("SELECT * FROM user WHERE uid=:uid LIMIT 1")
    User getUser(String uid);

    @Insert
    void insertUsers(User... users);

    @Delete
    void deleteUser(User user);
}
