package mirloupstmars.projetl3.data.model;

public class ImageModel {
    private String ext, url;

    public ImageModel(String url) {
        this.ext = url.substring(url.lastIndexOf('.') + 1);
        this.url = url;
    }

    public String getExt() {
        return ext;
    }

    public String getUrl() {
        return url;
    }
}
