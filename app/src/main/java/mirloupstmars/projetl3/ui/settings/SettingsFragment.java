package mirloupstmars.projetl3.ui.settings;

import android.os.Bundle;

import androidx.preference.PreferenceFragmentCompat;

import mirloupstmars.projetl3.R;

public class SettingsFragment extends PreferenceFragmentCompat {
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.preferences, rootKey);
    }
}