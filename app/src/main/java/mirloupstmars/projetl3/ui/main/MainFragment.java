package mirloupstmars.projetl3.ui.main;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.ui.NavigationUI;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import mirloupstmars.projetl3.R;
import mirloupstmars.projetl3.databinding.MainFragmentBinding;

import static androidx.navigation.fragment.NavHostFragment.findNavController;

public class MainFragment extends Fragment {
    private static final int REQUEST_TAKE_PHOTO = 1;

    MainViewModel mViewModel;
    private MainFragmentBinding binding;
    private MainViewModelFactory mViewModelFactory;

    private File storageDir = null;
    private String currentPhotoPath;
    private boolean online = false;

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        if (!storageDir.exists()) storageDir.mkdirs();
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(requireActivity().getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(requireContext(),
                        "mirloupstmars.projetl3.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO)
            if (resultCode == Activity.RESULT_OK) {
                mViewModel.downloadImages(online, storageDir);
            } else if (resultCode == Activity.RESULT_CANCELED) {
                if (currentPhotoPath != null)
                    new File(currentPhotoPath).delete();
                mViewModel.downloadImages(online, storageDir);
            }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.main_fragment, container, false);
        mViewModelFactory = new MainViewModelFactory(requireActivity().getApplication());
        mViewModel = new ViewModelProvider(this, mViewModelFactory).get(MainViewModel.class);
        online = getArguments() != null && MainFragmentArgs.fromBundle(getArguments()).getOnline();
        binding.setMainViewModel(mViewModel);
        binding.setLifecycleOwner(getViewLifecycleOwner());
        binding.fab.setOnClickListener(v -> dispatchTakePictureIntent());
        setHasOptionsMenu(true);
        storageDir = new File(requireContext().getExternalFilesDir(null), "images/" + MainFragmentArgs.fromBundle(getArguments()).getDisplayName());
        mViewModel.downloadImages(online, storageDir);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModelFactory = new MainViewModelFactory(requireActivity().getApplication());
        mViewModel = new ViewModelProvider(this, mViewModelFactory).get(MainViewModel.class);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        if (getArguments() != null && MainFragmentArgs.fromBundle(getArguments()).getOnline()) {
            inflater.inflate(R.menu.main_menu_online, menu);
        } else {
            inflater.inflate(R.menu.main_menu_offline, menu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_login:
                findNavController(this).navigate(MainFragmentDirections.actionLogin());
                return true;
            case R.id.menu_item_logout:
                findNavController(this).navigateUp();
                return true;
            case R.id.menu_item_settings:
                findNavController(this).navigate(MainFragmentDirections.actionSettings());
                return true;
        }
        return NavigationUI.onNavDestinationSelected(item, findNavController(this)) || super.onOptionsItemSelected(item);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        new RVAdapter(binding.recyclerView, this);
    }
}
