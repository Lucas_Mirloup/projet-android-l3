package mirloupstmars.projetl3.ui.main;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.navigation.ui.NavigationUI;
import androidx.preference.PreferenceManager;

import mirloupstmars.projetl3.R;

import static androidx.navigation.Navigation.findNavController;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        NavigationUI.setupActionBarWithNavController(this, findNavController(this, R.id.navHostFragment));
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        int nightMode = Integer.parseInt(sp.getString("dark_theme", "0"));
        if (nightMode == 0)
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);
        else if (nightMode == 1)
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        else if (nightMode == 2)
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        sp.registerOnSharedPreferenceChangeListener((sharedPreferences, key) -> {
            int nightModeModified = Integer.parseInt(sharedPreferences.getString(key, "0"));
            if (nightModeModified == 0)
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);
            else if (nightModeModified == 1)
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            else if (nightModeModified == 2)
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        return findNavController(this, R.id.navHostFragment).navigateUp();
    }
}
