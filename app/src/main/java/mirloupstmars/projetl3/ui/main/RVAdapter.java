package mirloupstmars.projetl3.ui.main;

import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.Objects;

import mirloupstmars.projetl3.R;
import mirloupstmars.projetl3.data.model.ImageModel;

import static androidx.navigation.fragment.NavHostFragment.findNavController;

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.MyViewHolder> {
    private final RecyclerView recyclerView;
    private final MainFragment frag;

    RVAdapter(RecyclerView recyclerView, MainFragment frag) {
        this.recyclerView = recyclerView;
        this.frag = frag;
        int spanCount = isPortrait() ? 3 : 5;
        recyclerView.setLayoutManager(new GridLayoutManager(recyclerView.getContext(), spanCount));
        recyclerView.setAdapter(this);
    }

    private boolean isPortrait() {
        DisplayMetrics dm = recyclerView.getContext().getResources().getDisplayMetrics();
        return dm.widthPixels < dm.heightPixels;
    }

    @NonNull
    @Override
    public RVAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ImageModel image = Objects.requireNonNull(frag.mViewModel.getDownloadedImages().getValue()).get(position);
        Glide.with(recyclerView).load(image.getUrl()).error(R.drawable.ic_error).into(holder.image);
        holder.image.setOnClickListener(new RVOnClick(image, frag));
    }

    @Override
    public int getItemCount() {
        return Objects.requireNonNull(frag.mViewModel.getDownloadedImages().getValue()).size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView image = itemView.findViewById(R.id.image);

        private MyViewHolder(View itemView) {
            super(itemView);
        }
    }

    static class RVOnClick implements View.OnClickListener {
        private ImageModel img;
        private MainFragment frag;

        RVOnClick(ImageModel img, MainFragment frag) {
            this.img = img;
            this.frag = frag;
        }

        @Override
        public void onClick(View v) {
            findNavController(frag).navigate(MainFragmentDirections.actionViewImage(img.getExt(), img.getUrl()));
        }
    }
}
