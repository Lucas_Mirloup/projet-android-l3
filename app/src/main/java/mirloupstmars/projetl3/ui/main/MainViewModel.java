package mirloupstmars.projetl3.ui.main;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.io.File;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import mirloupstmars.projetl3.R;
import mirloupstmars.projetl3.data.DownloadImages;
import mirloupstmars.projetl3.data.model.ImageModel;

public class MainViewModel extends AndroidViewModel {
    private final static String url = "https://picsum.photos/v2/list?limit=30";
    private MutableLiveData<ArrayList<ImageModel>> downloadedImages = new MutableLiveData<>(new ArrayList<>());

    MainViewModel(@NonNull Application application) {
        super(application);
    }

    LiveData<ArrayList<ImageModel>> getDownloadedImages() {
        return downloadedImages;
    }

    void downloadImages(boolean online, @Nullable File storageDir) {
        try {
            ArrayList<ImageModel> res = new ArrayList<>();
            if (storageDir.exists()) {
                for (File file : Objects.requireNonNull(storageDir.listFiles()))
                    res.add(new ImageModel(file.toURI().toURL().toString()));
                downloadedImages.setValue(res);
            }
            if (!online) {
                ConnectivityManager cm = (ConnectivityManager) getApplication().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = Objects.requireNonNull(cm).getActiveNetworkInfo();
                if (activeNetwork != null && activeNetwork.isConnectedOrConnecting())
                    res.addAll(new DownloadImages().execute(url).get());
                else {
                    res.add(new ImageModel(""));
                    downloadedImages.setValue(res);
                    Toast.makeText(getApplication().getApplicationContext(), R.string.not_connected, Toast.LENGTH_LONG).show();
                }
            }
        } catch (MalformedURLException | ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
